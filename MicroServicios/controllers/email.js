const nodemailer = require('nodemailer');
const mailConfig = require('../config/mailOwner');
const emailOptions = require('../class/emailOptions').emailOptions;
const emailOptionsWithTemplate = require('../class/emailOptionsWithTemplate').emailOptionsWithTemplate;
const hbs = require('nodemailer-express-handlebars');
const hbsOptions = require('../class/hbsOptions');

exports.send = (req, res) => {
	const transporter = nodemailer.createTransport(mailConfig);
	const mailOptions = new emailOptions(req.body);
	transporter.sendMail(mailOptions)
		.then(response => {
			res.IngeitResponse(response);
		})
		.catch(err => res.IngeitError(err));
}

exports.sendWithTemplate = (req, res) => {
	const transporter = nodemailer.createTransport(mailConfig);
	const mailOptions = new emailOptionsWithTemplate(req.body);
	transporter.use('compile', hbs(hbsOptions));
	transporter.sendMail(mailOptions)
		.then(response => {
			res.IngeitResponse(response);
		})
		.catch(err => res.IngeitError(err));
	// console.log(req.body);
	// res.render('email/test', {data:req.body.context}); //postman
}
