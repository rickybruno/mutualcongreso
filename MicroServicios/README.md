
# IngeitMailer

IngeitMailer cuenta con dos metodos, uno para mandarle el html y otro para usar template, ejemplo para usar el motodo template desde postman.

    post: localhost:3000/api/email

**headers**

    Content-Type: application/json
**body**

    {
    	"to": "masterk63@gmail.com",
    	"subject": "test",
    	"template":"default",//nombre del template, cambia segun el template a usar.
    	"context": {
    		"data": [
    		    { "username": "kevin", "email": "gomee" }
    		  ]
    	}
    }
