const express = require('express');
const router = express.Router();
require('../class/IngeitResponse').IngeitResponse((router));

// exportaciones modelos
const emails = require('./emails');

// Rutas
router.use('/email', emails);

module.exports = router;