
class emailOptions {
	constructor({ to, subject, html }) {
		//El from siempre se hardcodea, ya que el microservicio y el template se adaptan segun cliente.
		this.from = 'Cliente <cliente@gmail.com>';
		this.to = to;
		this.subject = subject;
		this.html = html;
	}
}

module.exports.emailOptions = emailOptions;
