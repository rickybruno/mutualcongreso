module.exports.IngeitResponse = router => {
    router.use(function (req, res, next) {
        res.IngeitResponse = function (respuesta) {
            return res.json([{ codigo: 1, mensaje: 'OK' }, [respuesta]]);
        };
        res.IngeitError = function (respuesta) {
            return res.json([{ codigo: -500, mensaje: `mensaje': Error numero: 500. Descripcion: ${respuesta}`},[]]);
        };
        next();
    });
}