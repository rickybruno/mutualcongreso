// aqui van los metodos utilizados como ser Transoformar fechas, y cosas asi q se puedan utilizar en varios lados
// como por ej tb Primera Letra Mayuscula

var simplecrypt = require("simplecrypt");
var sc = simplecrypt({ password: 'Ingeit2017!', salt: "10", method: "aes256" });
var jwt = require('jsonwebtoken');
var environment_var = require('../config/environment_var');
global.moment = require('moment');
moment.locale('es');
global.timestamp = moment().format('DD/MM/YYYY HH:mm:ss');// moment global para usar en cualquier lado sin importarla cada vez.

//esta funcion no esta en uso, se uso encambio, la misma pero sincronica, directamente en el controller por cuestiones de implementacion de AUTH.
exports.encriptarPassword = (password = '') => {
    return new Promise((resolve, reject) => {
        if (password.length == 0) {
            var respuesta = [{ 'codigo': 0, 'mensaje': "Debe ingresar una contraseña" }]
            return reject(respuesta);
        }
        var digest = sc.encrypt(password);
        resolve(digest);
    })
}

exports.createToken = (usuario) => {
	console.log('TCL: exports.createToken -> usuario', usuario)
    let payload = {
        idUsuario: usuario.idUsuario,
        username: usuario.username,
        razonSocial: usuario.razonSocial,
        cuit: usuario.cuit,
    };
    console.log('TCL: exports.createToken -> payload', payload)
    // let expires = { expiresIn: '1h' }
    // return jwt.sign(payload, environment_var.secret_key, expires); // con expire
    return jwt.sign(payload, environment_var.secret_key);
};

exports.verifyToken = (token) => {
    return jwt.verify(token, environment_var.secret_key, function (err, decoded) {
        if (err) return Promise.reject(err)
        return Promise.resolve(decoded)
    })
};