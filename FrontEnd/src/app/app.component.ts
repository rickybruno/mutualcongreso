import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Institucional',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Servicios',
      url: '/services-list',
      icon: 'cog'
    },
    {
      title: 'Beneficios',
      url: '/benefits',
      icon: 'trending-up'
    },
    {
      title: 'Asociate',
      url: '/subscribe',
      icon: 'clipboard'
    },
    {
      title:'Contacto',
      url:'/contact',
      icon :'information-circle-outline'
    }
  ];
  public pageSelected : string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private dataSrv: DataService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.pageSelected = window.location.pathname;
  }
  selectPage(urlPage){
    this.pageSelected = urlPage;
  }
}
