interface IArticle{
     _idArticle: number;
     _title: string;
     _description : string;
     _urlImg : string;
}
export class Article implements IArticle{
     _idArticle: number;
     _title: string;
     _description : string;
     _urlImg : string;

    constructor(pId : number,pTitle : string,pDescription : string,pUrlImg: string){
        this._idArticle = pId;
        this._title = pTitle;
        this._description = pDescription;
        this._urlImg = pUrlImg;
    }
    // properties:
    get id() : number{
        return this._idArticle;
    }
    set id(pId:number){
        this._idArticle = pId
    }

    get title() : string{
        return this._title;
    }
    set title(pTitle:string){
        this._title = pTitle;
    }
    get description() : string{
        return this._description;
    }
    set description(pDescription:string){
        this._description = pDescription
    }
    get urlImage() : string{
        return this._urlImg;
    }
    set urlImage(pUrlImg:string){
        this._urlImg = pUrlImg;
    }
    // end properties
}
