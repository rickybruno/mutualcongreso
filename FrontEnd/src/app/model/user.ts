interface IUser{
    _idUser: number;
    _name: string;
    _lastName: string;
    _dni: string;
    _phone: string;
    _email: string;
    _address: string;
    _country: string;
    _city: string;
    _postCode: number;
    _birthdate : string;
    _birthplace: string;
    _nacionalitiy:string;
    _maritalStatus:string;
    _mobilePhone : string;
    _workAddress : string;
    _workPhone: string; 
}
export class User implements IUser{
    _idUser: number;
    _name: string;
    _lastName: string;
    _dni: string;
    _phone: string;
    _email: string;
    _address: string;
    _country: string;
    _city: string;
    _postCode: number;
    _birthdate : string;
    _birthplace: string;
    _nacionalitiy:string;
    _maritalStatus:string;
    _mobilePhone : string;
    _workAddress : string;
    _workPhone: string; 

    // private _message: string; 
    // constructor(pIdUser: number, pName: string, pLastName: string, pDni: string, pPhone: string, pEmail: string, pAddress: string, pCountry: string, pCity: string, pPostCode: number) {
    //     this._idUser = pIdUser;
    //     this._name = pName;
    //     this._lastName = pLastName;
    //     this._dni = pDni;
    //     this._phone = pPhone;
    //     this._email = pEmail;
    //     this._address = pAddress;
    //     this._country = pCountry;
    //     this._city = pCity;
    //     this._postCode = pPostCode;
    // }
    constructor(){ 

    }
    get id(): number {
        return this._idUser;
    }
    set id(pId: number) {
        this._idUser = pId;
    }

    get name(): string {
        return this._name;
    }
    set name(pName : string) {
        this._name = pName;
    }
  
    get lastName(): string {
        return this._lastName;
    }
    set lastName(pLastName: string) {
        this._lastName = pLastName;
    }

    get dni(): string {
        return this._dni;
    }
    set dni(pDni: string) {
        this._dni = pDni;
    }
    get phone(): string {
        return this._phone;
    }
    set phone(pPhone: string) {
        this._phone = pPhone;
    }
    get email(): string {
        return this._email;
    }
    set email(pEmail: string) {
        this._email = pEmail;
    }
    get address(): string {
        return this._address;
    }
    set address(pAddress: string) {
        this._address = pAddress;
    }
    get country(): string {
        return this._country;
    }
    set country(pCountry: string) {
        this._country = pCountry;
    }
    get city(): string {
        return this._city;
    }
    set city(pCity: string) {
        this._city = pCity;
    }
    get postCode(): number {
        return this._postCode;
    }
    set postCode(pPostcode: number) {
        this._postCode = pPostcode;
    }
    get birthdate(): string {
        return this._birthdate;
    }
    set birthdate(birthdate: string) {
        this._birthdate = birthdate;
    }
    get birthplace(): string {
        return this._birthplace;
    }
    set birthplace(birthplace: string) {
        this._birthplace = birthplace;
    }
    get nacionalitiy(): string {
        return this._nacionalitiy;
    }
    set nacionalitiy(nacionalitiy: string) {
        this._nacionalitiy = nacionalitiy;
    }
    get maritalStatus(): string {
        return this._maritalStatus;
    }
    set maritalStatus(maritalStatus: string) {
        this._maritalStatus = maritalStatus;
    }   
     get mobilePhone(): string {
        return this._mobilePhone;
    }
    set mobilePhone(mobilePhone: string) {
        this._mobilePhone = mobilePhone;
    }
    get workAddress(): string {
        return this._workAddress;
    }
    set workAddress(workAddress: string) {
        this._workAddress = workAddress;
    }
    get workPhone(): string {
        return this._workPhone;
    }
    set workPhone(workPhone: string) {
        this._workPhone = workPhone;
    }

}