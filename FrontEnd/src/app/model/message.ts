import { User } from './user';
import * as moment from 'moment';
interface IMessage{
     _user: User;
     _messageBody : string;
     _dateTime : string;
}
export class Message implements IMessage{
     _user: User;
     _messageBody : string;
     _dateTime : string;

    constructor(){
        this._user = new User();
        this._messageBody = null;
        //this._dateTime = new Date();
        this._dateTime = null;
      //  console.log(this._dateTime);
    }

    get user(): User {
        return this._user;
    }
    set user(pUser: User) {
        this._user = pUser;
    }

    get body (): string {
        return this._messageBody;
    }
    set body(pMessageBody: string) {
        this._messageBody = pMessageBody;
    }
    get dateTime() : string{
        if(this._dateTime == null){
            this._dateTime = moment().format();
        }
        return this._dateTime;
    }
    set dateTime(dateTime:string) {
        if(dateTime != ''){
         this._dateTime = dateTime;
        }else{
         this._dateTime = moment().format();
        }
       // this._messageBody = pMessageBody;
    }
}