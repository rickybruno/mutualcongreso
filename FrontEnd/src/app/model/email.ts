import { Message } from './message';
import { subject, template, to } from '../config';
interface Iemail {
    to: string;
    subject: string;
    template: string;
    context: Message;
}
export class Email implements Iemail {
    to: string;
    subject: string;
    template: string;
    context: Message;
    constructor(pMessage: Message) {
        this.to = to;
        this.subject = subject;
        this.template = template;
        this.context = pMessage;
    }
}