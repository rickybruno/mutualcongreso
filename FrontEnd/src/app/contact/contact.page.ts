import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  public contactList = [
    {
      title: 'Línea Directa',
      label: '0810 555 4286',
      contactData: 'tel:+08105554286',
      icon: 'call',
      iconColor: 'phone'
    },
    {
      title: 'Whatsapp',
      label: '381 604-9771',
      contactData: 'tel:+3816049771',
      icon: 'logo-whatsapp',
      iconColor: 'whatsapp'
    },
    {
      title: 'Facebook',
      label : '/MutualCongreso',
      contactData: 'https://www.facebook.com/MutualCongreso/',
      icon: 'logo-facebook',
      iconColor: 'facebook'
    },
    {
      title: 'Instagram',
      label:'/mutual_congreso',
      contactData: 'https://www.instagram.com/mutual_congreso/',
      icon: 'logo-instagram',
      iconColor: 'instagram'
    }
  ]
  public offices = [
    {
      city:"Tucumán",
      address: "San Miguel de Tucumán: Lamadrid 456 3° “A”"
    },
    {
      city:"Jujuy",
      address:"Libertador Gral. San Martín: B° Centro Tucumán 309"
    },
    {
      city:"Catamarca",
      address:"San Fernando del Valle de Catamarca: San Martín 971"
    },
    {
      city:"Buenos Aires",
      address:"Contactanos por Facebook o email"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
