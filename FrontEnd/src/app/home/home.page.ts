import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public list = [
    {
      title: 'Misión',
      body: ' Brindar a nuestros asociados todos los servicios posibles para mejorar su calidad de vida tanto a nivel social, familiar y laboral. Así como también ser su apoyo económico y financiero.',
      icon: 'paper-plane',
      url: ''
    },
    {
      title: 'Visión',
      body: 'Mejorar día a día con más y mejores servicios brindarles seguridad y apoyo, que sientan que forman parte de esta gran familia mutualista.',
      icon: 'rocket',
      url: ''
    },
    {
      title: 'Valores',
      body: ' Solidaridad, respeto y empatía.​  ',
      icon: 'medal',
      url: ''
    }
  ]
  constructor(public dataSrv : DataService) {}
}
