import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService} from '../services/data.service';
@Component({
  selector: 'app-modal-commerces',
  templateUrl: './modal-commerces.page.html',
  styleUrls: ['./modal-commerces.page.scss'],
})
export class ModalCommercesPage implements OnInit {

  constructor(private modalController : ModalController,public dataSrv : DataService) { }

  ngOnInit() {}
  
  exit(){
    this.modalController.dismiss();
  }
}
