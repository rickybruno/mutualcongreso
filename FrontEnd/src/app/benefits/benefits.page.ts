import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ModalCommercesPage } from '../modal-commerces/modal-commerces.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.page.html',
  styleUrls: ['./benefits.page.scss'],
})
export class BenefitsPage implements OnInit {

  constructor(public dataSrv: DataService, private modalController: ModalController) { }
  
  ngOnInit() {
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalCommercesPage
    });
    await modal.present();
  }
}
