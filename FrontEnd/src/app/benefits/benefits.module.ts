import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { BenefitsPage } from './benefits.page';
import { ModalCommercesPage } from '../modal-commerces/modal-commerces.page';

const routes: Routes = [
  {
    path: '',
    component: BenefitsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    // ModalCommercesPageModule
  ],
  declarations: [BenefitsPage,ModalCommercesPage],
  entryComponents:[ModalCommercesPage]
})
export class BenefitsPageModule {}
