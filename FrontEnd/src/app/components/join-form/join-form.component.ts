import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Message } from '../../model/message';
import { User } from '../../model/user';
import { SubscribeService } from '../../subscribe/subscribe.service';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-join-form',
  templateUrl: './join-form.component.html',
  styleUrls: ['./join-form.component.scss'],
})
export class JoinFormComponent implements OnInit {
  public subscribeForm: FormGroup;
  private message: Message;
  public submitError: boolean;
  constructor(
    private formBuilder: FormBuilder,
    public toastController: ToastController,
    private SubscribeSrv: SubscribeService,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) {
    this.submitError = false;
    this.message = new Message();
    this.subscribeForm = this.formBuilder.group({
      name: ['',Validators.required],
      lastName: ['',Validators.required],
      dni: ['', Validators.required],
      birthplace: ['', Validators.required],
      birthdate: ['', Validators.required],
      nacionalitiy: ['', Validators.required],
      maritalStatus: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [ Validators.required, Validators.email]],
      phone: ['', ],
      mobilePhone: ['',Validators.required],
      workAddress: ['', Validators.required],
      workPhone: ['', Validators.required],
      bodyMessage: ['']
    });
  }

  ngOnInit() { }


  sendMessage() {

    if (this.subscribeForm.invalid) {
        this.submitError = true;
      console.log(this.subscribeForm.controls.name.invalid);
      return;
    } else {
      this.submitError = false;
      this.presentLoading().then(() => {
        this.loadMessage(this.subscribeForm.value);
        this.SubscribeSrv.sendForm(this.message)
          .then((res) => {
            // console.log(res);
            this.loadingController.dismiss();
            this.subscribeForm.reset();
            this.presentToastWithOptions("Solicitud enviada correctamente");
          })
          .catch((err) => {
            this.loadingController.dismiss();
            //   console.log(err);
            this.presentAlert('Intente Mas tarde.');
          });
      });
    }
  }
  async presentToastWithOptions(Cadena: string) {
    const toast = await this.toastController.create({
      message: Cadena,
      showCloseButton: true,
      position: 'bottom',
      closeButtonText: 'ok'
    });
    toast.present();
  }
  async presentAlert(Cadena: string) {
    const alert = await this.alertController.create({
      header: 'Error',
      subHeader: 'Se ha producido un error',
      message: Cadena,
      buttons: ['OK']
    });

    await alert.present();
  }

  private loadMessage(obj): void {
    /**
     * this function loads the data of a Message when the data provide a form.
     */
    // region message
    this.message.body = obj.bodyMessage;
    this.message.dateTime = '';// property on model load datetime.
    // region user
    this.message.user.name = obj.name;
    this.message.user.lastName = obj.lastName;
    this.message.user.dni = obj.dni;
    this.message.user.birthplace = obj.birthplace;
    this.message.user.birthdate = obj.birthdate;
    this.message.user.nacionalitiy = obj.nacionalitiy;
    this.message.user.maritalStatus = obj.maritalStatus;
    this.message.user.address = obj.address;
    this.message.user.email = obj.email;
    this.message.user.phone = obj.phone;
    this.message.user.mobilePhone = obj.mobilePhone;
    this.message.user.workAddress = obj.workAddress;
    this.message.user.workPhone = obj.workPhone;
    // end region user
    // end region message
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Enviando suscripción...',
      //    duration: 10000
    });
    await loading.present();
  }

}
