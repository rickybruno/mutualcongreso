import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesListPage } from './services-list.page';

describe('ServicesListPage', () => {
  let component: ServicesListPage;
  let fixture: ComponentFixture<ServicesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
