import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseURLBackend } from '../config'
@Injectable({
  providedIn: 'root'
})

export class DataService {
  private _url : string ;
  public imagesHome: Array<any> = [];
  public serviceList: Array<any> = [];
  public benefitsList: Array<any> = [];
  public commercesList: Array<any> = [];
  public logo : any;

  constructor(public http: HttpClient) {
    this.load();
   }

  load(){
    this._url = `${baseURLBackend}/data`;
    this.getData()
    .then((data:any)=>{
      this.imagesHome = data.respuesta[0].imagesHome;
      this.serviceList = data.respuesta[0].serviceList;
      this.benefitsList = data.respuesta[0].benefitsList;
      this.commercesList = data.respuesta[0].commercesList;
      this.logo = data.respuesta[0].logo;      
    })
    .catch(err=>{
      console.log(err);
    });
  }
  getData(){
     return this.http.get(this._url).toPromise();
  }
}
