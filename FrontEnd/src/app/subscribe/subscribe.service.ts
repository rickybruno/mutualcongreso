import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Message } from '../model/message';
import {baseURL} from '../config';
import {Email} from '../model/email'

const headers = new HttpHeaders();
headers.append('Content-Type', 'application/x-www-form-urlencoded');

@Injectable({
  providedIn: 'root'
})

export class SubscribeService {
  private _url : string ;

  constructor(  private http: HttpClient ) { 
    this.init();
  }
  init(){
    this._url = `${baseURL}/email`;
  }
  sendForm(pMessage : Message) {
    const _params = new Email(pMessage);
    // console.log(_params);
    return  this.http.post(this._url,_params,{ headers: headers }).toPromise();
  }
}
