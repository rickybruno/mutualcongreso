(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["services-list-services-list-module"],{

/***/ "./src/app/services-list/services-list.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/services-list/services-list.module.ts ***!
  \*******************************************************/
/*! exports provided: ServicesListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesListPageModule", function() { return ServicesListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services-list.page */ "./src/app/services-list/services-list.page.ts");







var routes = [
    {
        path: '',
        component: _services_list_page__WEBPACK_IMPORTED_MODULE_6__["ServicesListPage"]
    }
];
var ServicesListPageModule = /** @class */ (function () {
    function ServicesListPageModule() {
    }
    ServicesListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_services_list_page__WEBPACK_IMPORTED_MODULE_6__["ServicesListPage"]]
        })
    ], ServicesListPageModule);
    return ServicesListPageModule;
}());



/***/ }),

/***/ "./src/app/services-list/services-list.page.html":
/*!*******************************************************!*\
  !*** ./src/app/services-list/services-list.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"--color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Servicios</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"dataSrv.serviceList\">\n  <ion-grid>\n    <ion-row *ngFor=\"let service of dataSrv.serviceList\">\n      <ion-card>\n        <ion-img [src]=\"service.url\"></ion-img>\n        <ion-card-header>\n          <ion-card-title style=\"font-size: 26px;\">\n            {{service.title}}\n          </ion-card-title>\n        </ion-card-header>\n        <ion-card-content>\n          {{service.body}}\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/services-list/services-list.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/services-list/services-list.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2VzLWxpc3Qvc2VydmljZXMtbGlzdC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/services-list/services-list.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/services-list/services-list.page.ts ***!
  \*****************************************************/
/*! exports provided: ServicesListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesListPage", function() { return ServicesListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/data.service */ "./src/app/services/data.service.ts");



var ServicesListPage = /** @class */ (function () {
    function ServicesListPage(dataSrv) {
        this.dataSrv = dataSrv;
    }
    ServicesListPage.prototype.ngOnInit = function () { };
    ServicesListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-services-list',
            template: __webpack_require__(/*! ./services-list.page.html */ "./src/app/services-list/services-list.page.html"),
            styles: [__webpack_require__(/*! ./services-list.page.scss */ "./src/app/services-list/services-list.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]])
    ], ServicesListPage);
    return ServicesListPage;
}());



/***/ })

}]);
//# sourceMappingURL=services-list-services-list-module.js.map