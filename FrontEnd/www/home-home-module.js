(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"--color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Institucional\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid style=\"padding:0\">\n    <ion-row>\n      <div class=\"galery-article\" *ngIf=\"dataSrv.imagesHome\">\n        <ion-slides pager=\"true\" scrollbar=\"true\" [options]=\"slideOpts\">\n          <ion-slide *ngFor=\"let image of dataSrv.imagesHome\">\n            <img [src]=\"image.url\">\n          </ion-slide>\n        </ion-slides>\n      </div>\n    </ion-row>\n    <ion-row class=\"ion-margin-top\">\n      <ion-card>\n        <ion-card-header class=\"ion-padding\">\n          <img [src]=\"dataSrv.logo\">\n          <ion-card-subtitle class=\"ion-margin-top\">¿QUIÉNES SOMOS?</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content class=\"ion-padding\">\n          <p class=\"ion-text-justify\">\n            Somos una asociación Mutual cuyos asociados inicialmente pertenecían al Poder Judicial Nacional y\n            de la provincia de Tucumán, debido a nuestra intachable trayectoria de hace más de 20 años y la\n            calidad en la prestación de servicios, nuestros beneficios han sido requeridos por el personal de\n            Administración Pública (Municipalidad, Provincial y Nacional).\n          </p>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n    <ion-row>\n      <ion-card>\n        <ion-card-content>\n          <div class=\"promo\" class=\"ion-padding\" *ngFor=\"let l of list\">\n            <ion-row>\n              <ion-col size=\"12\" class=\"ion-text-center\">\n                <ion-icon [name]=\"l.icon\" color=\"secondary\"></ion-icon>\n              </ion-col>\n              <ion-col size=\"12\" class=\"ion-text-uppercase ion-text-center\">\n                {{l.title}}\n              </ion-col>\n              <ion-col size=\"12\" class=\"ion-text-center\">\n                <p>{{l.body}}</p>\n              </ion-col>\n            </ion-row>\n          </div>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n.galery-article {\n  width: 100%;\n  height: 50%;\n  background-color: antiquewhite;\n  overflow-y: hidden; }\n\n.swiper-slide img {\n  height: 300px; }\n\nion-icon {\n  font-size: 50px !important; }\n\n#rowLogo {\n  padding-top: 5%;\n  padding-left: 3%;\n  padding-right: 3%;\n  padding-bottom: 5%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvbXV0dWFsY29uZ3Jlc28vRnJvbnRFbmQvc3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsOEJBQThCO0VBQzlCLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGFBQWEsRUFBQTs7QUFFZjtFQUNFLDBCQUEwQixFQUFBOztBQUU1QjtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lLWNhcmQgaW9uLWltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uZ2FsZXJ5LWFydGljbGV7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogYW50aXF1ZXdoaXRlO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG4gLy8gb3ZlcmZsb3cteDogc2Nyb2xsO1xufVxuLnN3aXBlci1zbGlkZSBpbWd7XG4gIGhlaWdodDogMzAwcHg7XG59XG5pb24taWNvbntcbiAgZm9udC1zaXplOiA1MHB4ICFpbXBvcnRhbnQ7XG59XG4jcm93TG9nb3tcbiAgcGFkZGluZy10b3A6IDUlO1xuICBwYWRkaW5nLWxlZnQ6IDMlO1xuICBwYWRkaW5nLXJpZ2h0OiAzJTtcbiAgcGFkZGluZy1ib3R0b206IDUlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/data.service */ "./src/app/services/data.service.ts");



var HomePage = /** @class */ (function () {
    function HomePage(dataSrv) {
        this.dataSrv = dataSrv;
        this.list = [
            {
                title: 'Misión',
                body: ' Brindar a nuestros asociados todos los servicios posibles para mejorar su calidad de vida tanto a nivel social, familiar y laboral. Así como también ser su apoyo económico y financiero.',
                icon: 'paper-plane',
                url: ''
            },
            {
                title: 'Visión',
                body: 'Mejorar día a día con más y mejores servicios brindarles seguridad y apoyo, que sientan que forman parte de esta gran familia mutualista.',
                icon: 'rocket',
                url: ''
            },
            {
                title: 'Valores',
                body: ' Solidaridad, respeto y empatía.​  ',
                icon: 'medal',
                url: ''
            }
        ];
    }
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map