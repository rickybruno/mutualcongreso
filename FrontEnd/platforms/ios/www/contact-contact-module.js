(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-contact-module"],{

/***/ "./src/app/contact/contact.module.ts":
/*!*******************************************!*\
  !*** ./src/app/contact/contact.module.ts ***!
  \*******************************************/
/*! exports provided: ContactPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact.page */ "./src/app/contact/contact.page.ts");







var routes = [
    {
        path: '',
        component: _contact_page__WEBPACK_IMPORTED_MODULE_6__["ContactPage"]
    }
];
var ContactPageModule = /** @class */ (function () {
    function ContactPageModule() {
    }
    ContactPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_contact_page__WEBPACK_IMPORTED_MODULE_6__["ContactPage"]]
        })
    ], ContactPageModule);
    return ContactPageModule;
}());



/***/ }),

/***/ "./src/app/contact/contact.page.html":
/*!*******************************************!*\
  !*** ./src/app/contact/contact.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"--color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Contacto</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content >\n  <ion-card>\n    <ion-card-header>\n      Información de Contactos:\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item *ngFor=\"let item of contactList\" [href]=\"item.contactData\">\n          <ion-icon [name]=\"item.icon\" size=\"large\" [color]=\"item.iconColor\"></ion-icon>\n          <h4 class=\"ion-padding\">{{ item.title}}:</h4>\n          <p>{{item.label}}</p>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-card>\n    <ion-card-header>\n        Nos encontrás en:\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-item *ngFor=\"let office of offices\" class=\"ion-margin-bottom ion-margin-top\">\n         <h5 class=\"ion-text-start\">{{office.city}} - {{office.address}}</h5>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <router-outlet></router-outlet>\n</ion-content>"

/***/ }),

/***/ "./src/app/contact/contact.page.scss":
/*!*******************************************!*\
  !*** ./src/app/contact/contact.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/** facebook **/\n.ion-color-facebook {\n  --ion-color-base: #3b5998;\n  --ion-color-base-rgb: 59,89,152;\n  --ion-color-contrast: #ffffff;\n  --ion-color-contrast-rgb: 255,255,255;\n  --ion-color-shade: #344e86;\n  --ion-color-tint: #4f6aa2; }\n.ion-color-instagram {\n  --ion-color-base: #e4405f;\n  --ion-color-base-rgb: 228,64,95;\n  --ion-color-contrast: #ffffff;\n  --ion-color-contrast-rgb: 255,255,255;\n  --ion-color-shade: #c93854;\n  --ion-color-tint: #e7536f; }\n.ion-color-whatsapp {\n  --ion-color-base: #25D366;\n  --ion-color-base-rgb: 37,211,102;\n  --ion-color-contrast: #000000;\n  --ion-color-contrast-rgb: 0,0,0;\n  --ion-color-shade: #21ba5a;\n  --ion-color-tint: #3bd775; }\n.ion-color-phone {\n  --ion-color-base: #410093;\n  --ion-color-base-rgb: 65,0,147;\n  --ion-color-contrast: #ffffff;\n  --ion-color-contrast-rgb: 255,255,255;\n  --ion-color-shade: #390081;\n  --ion-color-tint: #541a9e; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvbXV0dWFsY29uZ3Jlc28vRnJvbnRFbmQvc3JjL2FwcC9jb250YWN0L2NvbnRhY3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFFLGVBQUE7QUFDRjtFQUNFLHlCQUFpQjtFQUNqQiwrQkFBcUI7RUFDckIsNkJBQXFCO0VBQ3JCLHFDQUF5QjtFQUN6QiwwQkFBa0I7RUFDbEIseUJBQWlCLEVBQUE7QUFFbkI7RUFDSSx5QkFBaUI7RUFDakIsK0JBQXFCO0VBQ3JCLDZCQUFxQjtFQUNyQixxQ0FBeUI7RUFDekIsMEJBQWtCO0VBQ2xCLHlCQUFpQixFQUFBO0FBRXJCO0VBQ0kseUJBQWlCO0VBQ2pCLGdDQUFxQjtFQUNyQiw2QkFBcUI7RUFDckIsK0JBQXlCO0VBQ3pCLDBCQUFrQjtFQUNsQix5QkFBaUIsRUFBQTtBQUVyQjtFQUNJLHlCQUFpQjtFQUNqQiw4QkFBcUI7RUFDckIsNkJBQXFCO0VBQ3JCLHFDQUF5QjtFQUN6QiwwQkFBa0I7RUFDbEIseUJBQWlCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0L2NvbnRhY3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICAvKiogZmFjZWJvb2sgKiovXG4uaW9uLWNvbG9yLWZhY2Vib29re1xuICAtLWlvbi1jb2xvci1iYXNlOiAjM2I1OTk4O1xuICAtLWlvbi1jb2xvci1iYXNlLXJnYjogNTksODksMTUyO1xuICAtLWlvbi1jb2xvci1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiAyNTUsMjU1LDI1NTtcbiAgLS1pb24tY29sb3Itc2hhZGU6ICMzNDRlODY7XG4gIC0taW9uLWNvbG9yLXRpbnQ6ICM0ZjZhYTI7XG59XG4uaW9uLWNvbG9yLWluc3RhZ3JhbXtcbiAgICAtLWlvbi1jb2xvci1iYXNlOiAjZTQ0MDVmO1xuICAgIC0taW9uLWNvbG9yLWJhc2UtcmdiOiAyMjgsNjQsOTU7XG4gICAgLS1pb24tY29sb3ItY29udHJhc3Q6ICNmZmZmZmY7XG4gICAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiAyNTUsMjU1LDI1NTtcbiAgICAtLWlvbi1jb2xvci1zaGFkZTogI2M5Mzg1NDtcbiAgICAtLWlvbi1jb2xvci10aW50OiAjZTc1MzZmO1xufVxuLmlvbi1jb2xvci13aGF0c2FwcHtcbiAgICAtLWlvbi1jb2xvci1iYXNlOiAjMjVEMzY2O1xuICAgIC0taW9uLWNvbG9yLWJhc2UtcmdiOiAzNywyMTEsMTAyO1xuICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiAjMDAwMDAwO1xuICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0LXJnYjogMCwwLDA7XG4gICAgLS1pb24tY29sb3Itc2hhZGU6ICMyMWJhNWE7XG4gICAgLS1pb24tY29sb3ItdGludDogIzNiZDc3NTtcbn1cbi5pb24tY29sb3ItcGhvbmV7XG4gICAgLS1pb24tY29sb3ItYmFzZTogIzQxMDA5MztcbiAgICAtLWlvbi1jb2xvci1iYXNlLXJnYjogNjUsMCwxNDc7XG4gICAgLS1pb24tY29sb3ItY29udHJhc3Q6ICNmZmZmZmY7XG4gICAgLS1pb24tY29sb3ItY29udHJhc3QtcmdiOiAyNTUsMjU1LDI1NTtcbiAgICAtLWlvbi1jb2xvci1zaGFkZTogIzM5MDA4MTtcbiAgICAtLWlvbi1jb2xvci10aW50OiAjNTQxYTllO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/contact/contact.page.ts":
/*!*****************************************!*\
  !*** ./src/app/contact/contact.page.ts ***!
  \*****************************************/
/*! exports provided: ContactPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPage", function() { return ContactPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ContactPage = /** @class */ (function () {
    function ContactPage() {
        this.contactList = [
            {
                title: 'Línea Directa',
                label: '0810 555 4286',
                contactData: 'tel:+08105554286',
                icon: 'call',
                iconColor: 'phone'
            },
            {
                title: 'Whatsapp',
                label: '381 604-9771',
                contactData: 'tel:+3816049771',
                icon: 'logo-whatsapp',
                iconColor: 'whatsapp'
            },
            {
                title: 'Facebook',
                label: '/MutualCongreso',
                contactData: 'https://www.facebook.com/MutualCongreso/',
                icon: 'logo-facebook',
                iconColor: 'facebook'
            },
            {
                title: 'Instagram',
                label: '/mutual_congreso',
                contactData: 'https://www.instagram.com/mutual_congreso/',
                icon: 'logo-instagram',
                iconColor: 'instagram'
            }
        ];
        this.offices = [
            {
                city: "Tucumán",
                address: "San Miguel de Tucumán: Lamadrid 456 3° “A”"
            },
            {
                city: "Jujuy",
                address: "Libertador Gral. San Martín: B° Centro Tucumán 309"
            },
            {
                city: "Catamarca",
                address: "San Fernando del Valle de Catamarca: San Martín 971"
            },
            {
                city: "Buenos Aires",
                address: "Contactanos por Facebook o email"
            }
        ];
    }
    ContactPage.prototype.ngOnInit = function () {
    };
    ContactPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__(/*! ./contact.page.html */ "./src/app/contact/contact.page.html"),
            styles: [__webpack_require__(/*! ./contact.page.scss */ "./src/app/contact/contact.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ContactPage);
    return ContactPage;
}());



/***/ })

}]);
//# sourceMappingURL=contact-contact-module.js.map