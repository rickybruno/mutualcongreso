(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["benefits-benefits-module"],{

/***/ "./src/app/benefits/benefits.module.ts":
/*!*********************************************!*\
  !*** ./src/app/benefits/benefits.module.ts ***!
  \*********************************************/
/*! exports provided: BenefitsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BenefitsPageModule", function() { return BenefitsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _benefits_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./benefits.page */ "./src/app/benefits/benefits.page.ts");
/* harmony import */ var _modal_commerces_modal_commerces_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modal-commerces/modal-commerces.page */ "./src/app/modal-commerces/modal-commerces.page.ts");








var routes = [
    {
        path: '',
        component: _benefits_page__WEBPACK_IMPORTED_MODULE_6__["BenefitsPage"]
    }
];
var BenefitsPageModule = /** @class */ (function () {
    function BenefitsPageModule() {
    }
    BenefitsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            ],
            declarations: [_benefits_page__WEBPACK_IMPORTED_MODULE_6__["BenefitsPage"], _modal_commerces_modal_commerces_page__WEBPACK_IMPORTED_MODULE_7__["ModalCommercesPage"]],
            entryComponents: [_modal_commerces_modal_commerces_page__WEBPACK_IMPORTED_MODULE_7__["ModalCommercesPage"]]
        })
    ], BenefitsPageModule);
    return BenefitsPageModule;
}());



/***/ }),

/***/ "./src/app/benefits/benefits.page.html":
/*!*********************************************!*\
  !*** ./src/app/benefits/benefits.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"--color: white;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Beneficios</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid *ngIf=\"dataSrv.benefitsList\">\n    <ion-row *ngFor=\"let benefit of dataSrv.benefitsList\">\n      <ion-card>\n        <ion-img [src]=\"benefit.url\" *ngIf=\"benefit.url\"></ion-img>\n        <ion-card-header>\n          <ion-card-title style=\"font-size: 19px;\">\n            {{benefit.title}}\n          </ion-card-title>\n        </ion-card-header>\n        <ion-card-content>\n          {{benefit.body}}\n          <ion-toolbar *ngIf=\"benefit.title == 'COMERCIOS ADHERIDOS'\">\n              <ion-buttons slot=\"end\">\n                <ion-button color=\"tertiary\" (click)=\"presentModal()\">Ver más</ion-button>\n              </ion-buttons>\n            </ion-toolbar>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/benefits/benefits.page.scss":
/*!*********************************************!*\
  !*** ./src/app/benefits/benefits.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JlbmVmaXRzL2JlbmVmaXRzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/benefits/benefits.page.ts":
/*!*******************************************!*\
  !*** ./src/app/benefits/benefits.page.ts ***!
  \*******************************************/
/*! exports provided: BenefitsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BenefitsPage", function() { return BenefitsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var _modal_commerces_modal_commerces_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../modal-commerces/modal-commerces.page */ "./src/app/modal-commerces/modal-commerces.page.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var BenefitsPage = /** @class */ (function () {
    function BenefitsPage(dataSrv, modalController) {
        this.dataSrv = dataSrv;
        this.modalController = modalController;
    }
    BenefitsPage.prototype.ngOnInit = function () {
    };
    BenefitsPage.prototype.presentModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _modal_commerces_modal_commerces_page__WEBPACK_IMPORTED_MODULE_3__["ModalCommercesPage"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BenefitsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-benefits',
            template: __webpack_require__(/*! ./benefits.page.html */ "./src/app/benefits/benefits.page.html"),
            styles: [__webpack_require__(/*! ./benefits.page.scss */ "./src/app/benefits/benefits.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_data_service__WEBPACK_IMPORTED_MODULE_2__["DataService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])
    ], BenefitsPage);
    return BenefitsPage;
}());



/***/ }),

/***/ "./src/app/modal-commerces/modal-commerces.page.html":
/*!***********************************************************!*\
  !*** ./src/app/modal-commerces/modal-commerces.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Comercios Adheridos</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"exit()\">atrás</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <ion-grid *ngIf=\"dataSrv.commercesList\">\n    <ion-row>\n      <ion-col size=\"6\" *ngFor=\"let commerce of dataSrv.commercesList\">\n        <ion-card>\n          <a [attr.href]=\"commerce.urlRef !== '' ? commerce.urlRef : null\" target=\"_blank\">\n            <ion-img [src]=\"commerce.urlImage\"></ion-img>\n          </a>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/modal-commerces/modal-commerces.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/modal-commerces/modal-commerces.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFsLWNvbW1lcmNlcy9tb2RhbC1jb21tZXJjZXMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/modal-commerces/modal-commerces.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/modal-commerces/modal-commerces.page.ts ***!
  \*********************************************************/
/*! exports provided: ModalCommercesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCommercesPage", function() { return ModalCommercesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/data.service */ "./src/app/services/data.service.ts");




var ModalCommercesPage = /** @class */ (function () {
    function ModalCommercesPage(modalController, dataSrv) {
        this.modalController = modalController;
        this.dataSrv = dataSrv;
    }
    ModalCommercesPage.prototype.ngOnInit = function () { };
    ModalCommercesPage.prototype.exit = function () {
        this.modalController.dismiss();
    };
    ModalCommercesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-commerces',
            template: __webpack_require__(/*! ./modal-commerces.page.html */ "./src/app/modal-commerces/modal-commerces.page.html"),
            styles: [__webpack_require__(/*! ./modal-commerces.page.scss */ "./src/app/modal-commerces/modal-commerces.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], ModalCommercesPage);
    return ModalCommercesPage;
}());



/***/ })

}]);
//# sourceMappingURL=benefits-benefits-module.js.map