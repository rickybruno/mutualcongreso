
class emailOptionsWithTemplate {
	constructor({ to, subject, template, context }) {
		//El from siempre se hardcodea, ya que el microservicio y el template se adaptan segun cliente.
		this.from = 'Cliente <cliente@gmail.com>';
		this.to = to;
		this.subject = subject;
		this.template = template;
		this.context = context;
	}
}

module.exports.emailOptionsWithTemplate = emailOptionsWithTemplate;
