/* 
Setear constiables de entorno para NODEjs en CONSOLA:
WINDOWS: set NODE_ENV=production
UBUNTU: NODE_ENV=production o export NODE_ENV=production
*/
// const createError = require('http-errors');
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const cors = require('cors');
const apiRouter = require('./routes/api');
const helmet = require('helmet');
const response = require('./models/default/response')
const app = express();
const utils = require('./providers/utils');
var hbs = require('express-handlebars');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'pug');
app.set('view engine', 'hbs');

app.engine('hbs', hbs({
  //   extname: 'hbs',
  // //  defaultView: 'default',
  //   layoutsDir: __dirname + '/views/email/',
  //   partialsDir: __dirname + '/views/partials/email'
  // ----
  extname: '.hbs',
  layoutsDir: 'views/email/',
  defaultLayout: 'template',
  partialsDir: 'views/partials/'
}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.options('*', cors())
app.use(helmet());

/***************  Logs Morgan ***********************/
morgan.token('date', function () {
  return timestamp;
})
app.use(morgan(':date - URL ":method :url :status" - ORIGEN ":remote-addr :remote-user" - RESLENGTH ":res[content-length]" - :response-time ms'))
/****************************************************/

/*************** response y Routas ******************/
app.use(response);
// habiltiamos CORS OPTIONS al servidor
app.options('*', cors())
app.use('/api', apiRouter);
//Propia
app.use((req, res) => {
  res.status(404).send('Recurso no disponible');
});
/***************************************/

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json('error');
});

module.exports = app;
