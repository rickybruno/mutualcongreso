const dataList ={
  logo: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/02/logo-mutual-congreso2.png',
  imagesHome: [
    {
      title: 'nocheDebodas',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/1-1026x513.jpg'
    },
    {
      title: 'asesoriaLegal',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/2-1026x513.jpg'
    },
    {
      title: 'viajaConTuMutual',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/3-1026x513.jpg'
    },
    {
      title: 'subsidioPorNacimiento',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/4-1026x513.jpg'
    },
    {
      title: 'capacitaciones',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/5-1026x513.jpg'
    },
    {
      title: 'BeneficiosExclusivos',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/6-1026x513.jpg'
    }
  ],
  serviceList: [
    {
      title: 'TURISMO',
      body: 'Para beneficio de nuestros afiliados, gozamos de numerosos convenios con empresas de turismo, brindando mayores facilidades.',
      icon: 'airplane',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/descarga.jpg'
    },
    {
      title: 'ELECTRODOMESTICOS',
      body: 'Mutual Congreso viste tu hogar con amplia gama de electrodomésticos: heladeras, lavarropas, cocinas, audio y video, celulares, colchones y muebles, todo para tu hogar.',
      icon: 'desktop',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/cabecera-electrodomesticos.jpg'
    },
    {
      title: 'ASESORIA LEGAL',
      body: 'Para nuestros afiliados, brindamos el servicio de asesoramiento juridico.',
      icon: 'hammer',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/gavel.jpg'
    },
    {
      title: 'CAPACITACIONES',
      body: 'Brindamos servicios de educación, capacitación en general para nuestros afiliados.',
      icon: 'create',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/tres.jpg'
    },
    {
      title: 'ORDENES DE COMPRA',
      body: 'Para beneficio de nuestros asociados nos hemos puesto en contacto con distintos comercios.',
      icon: 'cart',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/turismo-de-compras-800x600.jpg'
    },
    {
      title: 'AHORRO',
      body: 'Te brindamos una forma rapida y segura para cuidar tus ahorros. Únicamente para nuestros afiliados en Tucumán.',
      icon: 'trending-up',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/80814aa9cdd954a633aa91b480e774a7-300x225.jpg'
    },
    {
      title: 'PRESTAMOS',
      body: 'Ofrecemos una amplia linea de préstamos a todos nuestros afiliados con una financiación hasta 30 cuotas fijas en pesos, con débito automático y códigos de descuentos a traves de diversos convenios.',
      icon: 'cash',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/diferencia-credito-prestamo-1.jpg'
    }
  ],
  benefitsList: [
    {
      title: 'SUBSIDIO POR NACIMIENTO',
      body: 'Se solicita hasta 45 días después de la fecha de nacimiento. Es requisito no registrar deudas impagas de ningún concepto con La Mutual y tener una antigüedad de 1 años como socio.',
      icon: 'medkit',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/Como-aliviar-el-dolor-de-dientes-y-encias-del-bebe.jpg'
    },
    {
      title: 'MATRIMONIO',
      body: 'Se solicita hasta 45 días después de la fecha del casamiento. Es requisito no registrar deudas impagas de ningún concepto con La Mutual y tener una antigüedad de 1 año como socio.',
      icon: 'heart-empty',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/momentos-iconicos-en-una-boda-fotografias-magicas-y-espontaneas-en-tu-casamiento-1.jpg'
    },
    {
      title: 'SUBSIDIO POR FALLECIMIENTO',
      body: 'Se solicita hasta 45 días después de la fecha de fallecimiento. Es requisito no registrar deudas impagas de ningún concepto con La Mutual y tener una antiguedad de 1 año como socio.',
      icon: 'filing',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/blanca-paloma.jpg'
    },
    {
      title: 'BONUSTICKET',
      body: 'Es un programa de Beneficios y Descuentos a través de la entrega de una cuponera de descuentos. Dicha Cuponera, permite, que los afiliados de La Mutual, puedan hacer compras con descuentos en Super, Farmacia, Cine, GYM, opticca, Heladeria, y mas de 45 comercios.',
      icon: 'cut',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/descuentos-vinilos-vitrinas-decorativo-personalizado-tiendas-D_NQ_NP_866992-MLA26963678245_032018-F.jpg'
    },
    {
      title: 'COMERCIOS ADHERIDOS',
      body: 'Emilio Luque, Yuhmak, Red Farmacia Plazoleta, Sencillo Creditos Personales, Grandes Pinturerias Silva, Blue Jeans, Studio, Set Point, Belinda Puntos sorteos.',
      icon: 'business',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/images.jpg'
    },
    {
      title: 'SORTEOS MENSUALES',
      body: 'Realizamos sorteos mensuales con fabulosos premios para nuestros afiliados.',
      icon: 'gift',
      url: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/regalos-de-emrpesa-navidad.jpg'
    }
  ],
  commercesList: [
    {
      title: 'Emilio Luque',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/10885511_1380055945630508_3264983874721817716_n-300x300.jpg',
      urlRef: 'http://www.emilio-luque.com.ar/main/home'
    },
    {
      title: 'Informatica Shark',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/logo-final-300x300.png',
      urlRef: 'http://sharkinformatica.com/'
    },
    {
      title: 'Yuhmak',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/34ANCEDc_400x400-300x300.jpeg',
      urlRef: 'http://ofertas.yuhmak.com.ar/'
    },
    {
      title: 'Mastri turismo',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/LOGO-pelotita-300x238.png',
      urlRef: ''
    },
    {
      title: 'TRG Training Group',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/30711155_2145839795646422_310844487311150061_n-300x300.jpg',
      urlRef: 'https://www.facebook.com/TRGcenter/'
    },
    {
      title: 'Romay Joyeria',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/romay-1-300x218.png',
      urlRef: 'https://www.facebook.com/joyeriaromay/'
    },
    {
      title: 'Romay Joyeria',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/romay2-300x208.png',
      urlRef: 'https://www.facebook.com/joyeriaromay/'
    },
    {
      title: 'Sencillo Creditos Personales',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/SENCILLO-logo-PNG-300x258.png',
      urlRef: ''
    },
    {
      title: 'Kavac Deportes',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/guante-the-big-tiger-fitness-neoprene-lycra-D_NQ_NP_851305-MLA25016859927_082016-F-300x300.jpg',
      urlRef: 'http://www.kavakdeportes.com.ar/'
    },
    {
      title: 'Farmacias Plazoleta',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/farmacia-plazoleta-300x294-300x294.jpg',
      urlRef: 'http://www.asispre.com/kyma_portfolio/farmacia-plazoleta/'
    },
    {
      title: 'Freesia',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/freesia-273x300.jpg',
      urlRef: ''
    },
    {
      title: 'Leon Libreria',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/48310065_1422645764534554_7231250223673114624_n-2-300x259.jpg',
      urlRef: 'https://www.facebook.com/librerialeon310/'
    },
    {
      title: 'Nadira',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/nadira.png',
      urlRef: 'https://www.facebook.com/Nadira-Belleza-y-Est%C3%A9tica-136890420271973/'
    },
    {
      title: 'Mundo Papel',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/Mundo-papel-300x267.jpg',
      urlRef: 'https://www.facebook.com/mundopapel.libreria'
    },
    {
      title: 'Sebastian Maturana',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/Logo-Tarjeta-291x300.png',
      urlRef: ''
    },
    {
      title: 'San Patricio',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/SAN-PATRICIO-LOGO-300x295.jpg',
      urlRef: 'https://www.facebook.com/alejandro.arjona.7965/'
    },
    {
      title: 'Optica Vision',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/index-1-1024x885.jpg',
      urlRef: ''
    },
    {
      title: 'Rocson',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/04/ROCSON-LOGO-1-300x265.jpg',
      urlRef: ''
    },
    {
      title: 'Opticas Buenos Aires',
      urlImage: 'http://mutualcongreso.com.ar/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-28-at-14.29.39.jpeg',
      urlRef: ''
    }
  ]
}
module.exports = dataList;