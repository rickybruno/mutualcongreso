const express = require('express');
const router = express.Router();
require('../class/IngeitResponse').IngeitResponse((router));

// exportaciones modelos
const data = require('./data');

// Rutas
router.use('/data', data);

module.exports = router;